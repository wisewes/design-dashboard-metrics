'use strict';

$(document).ready(function() {


  //line graph dataset
  var data = {
    labels: ['Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    datasets: [
      {
        label: '',
        fillColor: '#00BCE9',
        strokeColor: '#F15050',
        pointColor: '#F15050',
        pointStrokeColor: 'rgba(94,31,31,1)',
        pointHighlightFill: '#fff',
        pointHighlightStroke: '#F15050',
        data: [242, 236, 225, 234, 242, 235]
      }
    ]
  };

  //options for chart
  var options = {
    scaleShowGridLines: true,
    scaleGridLineColor: '#CCC',
    scaleGridLineWidth: 1,
    scaleShowHorizontalLines: true,
    scaleShowVerticalLines: true,
    bezierCurve: true,
    bezierCurveTension: 0.4,
    pointDot: true,
    pointDotRadius: 6,
    pointDotStrokeWidth: 4,
    pointHitDetectionRadius: 20,
    datasetStroke: true,
    datasetStrokeWidth: 6,
    datasetFill: true,
    legendTemplate: '<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
  };

  //canvas context and init chart
  var chartCanvas = $('#graph-chart').get(0).getContext('2d');
  var chart = new Chart(chartCanvas).Line(data, options);



});
